import React, { Component } from 'react';
import Navbar from './components/Navbar';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Home from './components/Home';
import Contact from './components/Contact';
import About from './components/About';
import Post from './components/Post';
import { CubeGrid } from 'styled-loaders-react';

class App extends Component {
  state = {
    loading: true,
  };

  componentDidMount = () => {
    setTimeout(() => {
      this.setState({
        loading: false,
      });
    }, 3000);
  };

  render() {
    return (
      <BrowserRouter>
        <div className='App'>
          {this.state.loading ? (
            <div class='loader'>
              <CubeGrid color='black' size='60px' duration='5s' />
            </div>
          ) : (
            <div>
              <Navbar />
              <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/about' component={About} />
                <Route path='/contact' component={Contact} />
                <Route path='/:post_id' component={Post} />
              </Switch>
            </div>
          )}
        </div>
      </BrowserRouter>
    );
  }
}
export default App;
